# Leetcode Problem Practice

Leetcode Problem Practice

Design Pattern

Data Structure


*   **LeetCode :**

        1. Two Sum (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/TwoSum_1.java)      [python](https://bitbucket.org/dodoggyy/leetcode/src/master/Py_src/)
     [C](https://bitbucket.org/dodoggyy/leetcode/src/master/C_src/)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/two_sum_1.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/twoSum_1.go)      [ref](https://leetcode.com/problems/two-sum/)
     
        2. Add Two Numbers (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/AddTwoNumbers_2.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/add_two_numbers_2.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/addTwoNumbers_2.go)      [ref](https://leetcode.com/problems/add-two-numbers/)
     
        3. Longest Substring Without Repeating Characters (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/LongestSubstringWithoutRepeatingCharacters_3.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/longest_substring_without_repeating_characters_3.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/longestSubstringWithoutRepeatingCharacters_3.go)      [ref](https://leetcode.com/problems/longest-substring-without-repeating-characters/)
     
        5. Longest Palindromic Substring (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/LongestPalindromicSubstring_5.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/longestPalindromicSubstring_5.go)      [ref](https://leetcode.com/problems/longest-palindromic-substring/)

        7. Reverse Integer (Easy)
     [java](https://github.com/dodoggyy/AlgorithmPractice/blob/master/src/com/practice/ReverseInteger.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/reverse_integer_7.cc)      [ref](https://leetcode.com/problems/reverse-integer/)

        8. String to Integer (atoi) (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/stringToInteger_8.go)      [ref](https://leetcode.com/problems/string-to-integer-atoi/)
     
        9. Palindrome Number (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/PalindromeNumber_9.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/palindrome_number_9.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/palindromeNumber_9.go)      [ref](https://leetcode.com/problems/palindrome-number/)
     
        11. Container With Most Water (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/ContainerWithMostWater_11.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/container_with_most_water_11.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/containerWithMostWater_11.go)      [ref](https://leetcode.com/problems/container-with-most-water/)
     
        14. Longest Common Prefix (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/LongestCommonPrefix_14.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/longest_common_prefix_14.cc)      [ref](https://leetcode.com/problems/longest-common-prefix/)
     
        15. 3Sum (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/ThreeSum_15.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/3_sum.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/3Sum_15.go)      [ref](https://leetcode.com/problems/3sum/)

        16. 3Sum Closest (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/3SumClosest_16.go)      [ref](https://leetcode.com/problems/3sum-closest/)
     
        17. Letter Combinations of a Phone Number (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/LetterCombinationsOfAPhoneNumber_17.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/letter_combinations_of_a_phone_number_17.cc)      [ref](https://leetcode.com/problems/letter-combinations-of-a-phone-number/)

        18. 4Sum (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/4Sum_18.go)      [ref](https://leetcode.com/problems/4sum/)
     
        19. Remove Nth Node From End of List (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/RemoveNthNodeFromEndofList_19.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/remove_nth_node_from_end_of_list_19.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/removeNthNodeFromEndOfList_19.go)      [ref](https://leetcode.com/problems/remove-nth-node-from-end-of-list/)

        20. Valid Parentheses (Easy)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/validParentheses_20.go)      [ref](https://leetcode.com/problems/valid-parentheses/)
     
        21. Merge Two Sorted Lists (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/MergeTwoSortedLists_21.java)      [C](https://bitbucket.org/dodoggyy/leetcode/src/master/C_src/Easy/21_Merge_Two_Sorted_Lists/MergeTwoSortedLists_21.c)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/merge_two_sorted_lists_21.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/mergeTwoSortedLists_21.go)      [ref](https://leetcode.com/problems/merge-two-sorted-lists/)
     
        22. Generate Parentheses (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/GenerateParentheses_22.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/mergeKSortedLists_23.go)      [ref](https://leetcode.com/problems/generate-parentheses/)
     
        23. Merge k Sorted Lists (Hard)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/hard/MergeKSortedLists_23.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/hard/mergeKSortedLists_23.go)      [ref](https://leetcode.com/problems/merge-k-sorted-lists/)
     
        24. Swap Nodes in Pairs (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/SwapNodesinPairs_24.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/swap_nodes_in_pairs_24.cc)      [ref](https://leetcode.com/problems/swap-nodes-in-pairs/)
     
        26. Remove Duplicates from Sorted Array (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/RemoveDuplicatesFromSortedArray_26.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/removeDuplicatesFromSortedArray_26.go)      [ref](https://leetcode.com/problems/remove-duplicates-from-sorted-array/)
     
        29. Divide Two Integers (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/DivideTwoIntegers_29.java)
     
        33. Search in Rotated Sorted Array (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/SearchInRotatedSortedArray_33.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/searchInRotatedSortedArray_33.go)      [ref](https://leetcode.com/problems/search-in-rotated-sorted-array/)
     
        34. Find First and Last Position of Element in Sorted Array (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/FindFirstAndLastPositionOfElementInSortedArray_34.java)      [ref](https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/)

        35. Search Insert Position (Easy)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/addB
     searchInsertPosition_35.go)      [ref](https://leetcode.com/problems/search-insert-position/)

        38. Count and Say (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/combinationSum_39.go)      [ref](https://leetcode.com/problems/count-and-say/)
     
        39. Combination Sum (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/CombinationSum_39.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/combinationSum_39.go)      [ref](https://leetcode.com/problems/combination-sum/)
     
        40. Combination Sum II (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/CombinationSumII_40.java)      [ref](https://leetcode.com/problems/combination-sum-ii/)

        41. First Missing Positive (Hard)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/hard/firstMissingPositive_41.go)      [ref](https://leetcode.com/problems/first-missing-positive/)

        43. Multiply Strings (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/multiplyStrings_43.go)      [ref](https://leetcode.com/problems/multiply-strings/)
     
        46. Permutations (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/Permutations_46.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/permutation_46.go)      [ref](https://leetcode.com/problems/permutations/)
     
        48. Rotate Image (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/RotateImage_48.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/rotate_image_48.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/rotateImage_48.go)      [ref](https://leetcode.com/problems/rotate-image/)

        49. Group Anagrams (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/groupAnagrams_49.go)      [ref](https://leetcode.com/problems/group-anagrams/)

        53. Maximum Subarray (Easy)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/maximumSubarray_53.go)      [ref](https://leetcode.com/problems/maximum-subarray/)
     
        54. Spiral Matrix (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/SpiralMatrix_54.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/spiralMatrix_54.go)      [ref](https://leetcode.com/problems/spiral-matrix/)

        55. Jump Game (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/mergeIntervals_56.go)      [ref](https://leetcode.com/problems/jump-game/)

        56. Merge Intervals (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/mergeIntervals_56.go)      [ref](https://leetcode.com/problems/merge-intervals/)

        57. Insert Interval (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/insertInterval_57.go)      [ref](https://leetcode.com/problems/insert-interval/)
     
        62. Unique Paths (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/UniquePaths_62.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/unique_paths_62.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/uniquePaths_62.go)      [ref](https://leetcode.com/problems/unique-paths/)

        63. Unique Paths II (Medium)
     [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/unique_paths_II_63.cc)      [ref](https://leetcode.com/problems/unique-paths-ii/)
     
        64. Minimum Path Sum (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/MinimumPathSum_64.java)

        66. Plus One (Easy)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/plusOne_66.go)      [ref](https://leetcode.com/problems/plus-one/)
     
        67. Add Binary (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/AddBinary_67.java)      [C](https://bitbucket.org/dodoggyy/leetcode/src/master/C_src/Easy/67_Add_Binary/AddBinary_67.c)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/add_binary_67.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/addB
     inary_67.go)      [ref](https://leetcode.com/problems/add-binary/)

        69. Sqrt(x) (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/Sqrtx_69.java)      [ref](https://leetcode.com/problems/sqrtx/)
     
        70. Climbing Stairs (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ClimbingStairs_70.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/climbingStairs_70.go)      [ref](https://leetcode.com/problems/climbing-stairs/)

        73. Set Matrix Zeroes (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/setMatrixZeroes_73.go)      [ref](https://leetcode.com/problems/set-matrix-zeroes/)
     
        75. Sort Colors (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/SortColors_75.java)      [ref](https://leetcode.com/problems/sort-colors/)
     
        78. Subsets (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/Subsets_78.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/subsets_78.go)      [ref](https://leetcode.com/problems/subsets/)
     
        79. Word Search (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/WordSearch_79.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/wordSearch_79.go)      [ref](https://leetcode.com/problems/word-search/)

        81. Search in Rotated Sorted Array II (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/searchInRotatedSortedArrayII_81.go)      [ref](https://leetcode.com/problems/search-in-rotated-sorted-array-ii/)
        
        83. Remove Duplicates from Sorted List (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/RemoveDuplicatesfromSortedList_83.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/remove_duplicates_from_sorted_list_83.cc)      [ref](https://leetcode.com/problems/remove-duplicates-from-sorted-list/)
     
        88. Merge Sorted Array (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/MergeSortedArray_88.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/merge_sorted_array_88.cc)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/mergeSortedArray_88.go)      [ref](https://leetcode.com/problems/merge-sorted-array/)
     
        91. Decode Ways (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/DecodeWays_91.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/decodeWays_91.go)      [ref](https://leetcode.com/problems/decode-ways/)
     
        92. Reverse Linked List II (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/ReverseLinkedListII_92.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/reverse_linked_list_II_92.cc)      [ref](https://leetcode.com/problems/reverse-linked-list-ii/)
     
        94. Binary Tree Inorder Traversal (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/BinaryTreeInorderTraversal_94.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/binaryTreeInorderTraversal_94.go)      [ref](https://leetcode.com/problems/binary-tree-inorder-traversal/)

        95. Unique Binary Search Trees II (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/uniqueBinarySearchTreesII_95.go)      [ref](https://leetcode.com/problems/unique-binary-search-trees-ii/description/)
     
        98. Validate Binary Search Tree (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/ValidateBinarySearchTree_98.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/validateBinarySearchTree_98.go)      [ref](https://leetcode.com/problems/validate-binary-search-tree/)
     
        100. Same Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/SameTree_100.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/sameTree_100.go)      [ref](https://leetcode.com/problems/same-tree/)
     
        101. Symmetric Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/SymmetricTree_101.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/symmetricTree_101.go)      [ref](https://leetcode.com/problems/symmetric-tree/)
     
        102. Binary Tree Level Order Traversal (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/BinaryTreeLevelOrderTraversal_102.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/binaryTreeLevelOrderTraversal_102.go)      [ref](https://leetcode.com/problems/binary-tree-level-order-traversal/)
     
        104. Maximum Depth of Binary Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/MaximumDepthofBinaryTree_104.java)      [C](https://bitbucket.org/dodoggyy/leetcode/src/master/C_src/Easy/104_Maximum_Depth_of_Binary_Tree/MaximumDepthOfBinaryTree_104.c)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/maximum_depth_of_binary_tree_104.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/maximumDepthOfBinaryTree_104.go)      [ref](https://leetcode.com/problems/maximum-depth-of-binary-tree/)

        105. Construct Binary Tree from Preorder and Inorder Traversal (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/constructBinaryTreeFromPreorderAndInorderTraversal_105.go)      [ref](https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/)
     
        106. Construct Binary Tree from Inorder and Postorder Traversal (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/constructBinaryTreeFromInorderAndPostorderTraversal_106.go)      [ref](https://leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/)

        107. Binary Tree Level Order Traversal II (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/BinaryTreeLevelOrderTraversalII_107.java)
     
        108. Convert Sorted Array to Binary Search Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ConvertSortedArraytoBinarySearchTree_108.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/convertSortedArrayToBinarySearchTree_108.go)      [ref](https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/)

        109. Convert Sorted List to Binary Search Tree (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/ConvertSortedListToBinarySearchTree_109.go)      [ref](https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/)
     
        110. Balanced Binary Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/BalancedBinaryTree_110.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/balanced_binary_tree_110.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/BalancedBinaryTree_110.go)      [ref](https://leetcode.com/problems/balanced-binary-tree/)
     
        111. Minimum Depth of Binary Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/MinimumDepthOfBinaryTree_111.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/minimumDepthOfBinaryTree_111.go)      [ref](https://leetcode.com/problems/minimum-depth-of-binary-tree/)
     
        112. Path Sum (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/PathSum_112.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/pathSum_112.go)      [ref](https://leetcode.com/problems/path-sum/)
     
        118. Pascal's Triangle (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/PascalsTriangle_118.java)
     
        119. Pascal's Triangle II (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/PascalsTriangleII_119.java)
     
        121. Best Time to Buy and Sell Stock (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/BestTimeToBuyAndSellStock_121.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/bestTimeToBuyAndSellStock_121.go)      [ref](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/solution/)
     
        122. Best Time to Buy and Sell Stock II (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/BestTimeToBuyAndSellStockII_122.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/bestTimeToBuyAndSellStock II_122.go)      [ref](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/)

        124. Binary Tree Maximum Path Sum (Hard)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/hard/binaryTreeMaximumPathSum_124.go)      [ref](https://leetcode.com/problems/binary-tree-maximum-path-sum/)
     
        125. Valid Palindrome (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ValidPalindrome_125.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/validPalindrome_125 II_122.go)      [ref](https://leetcode.com/problems/valid-palindrome/)

        127. Word Ladder (Hard)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/hard/wordLadder_127.go)      [ref](https://leetcode.com/problems/word-ladder/)

        128. Longest Consecutive Sequence (Hard)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/hard/longestConsecutiveSequence_128.go)      [ref](https://leetcode.com/problems/longest-consecutive-sequence/)

        133. Clone Graph (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/cloneGraph_133.go)      [ref](https://leetcode.com/problems/clone-graph/)
     
        136. Single Number (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/SingleNumber_136.java)      [C](https://bitbucket.org/dodoggyy/leetcode/src/master/C_src/Easy/136.%20Single%20Number/SingleNumber_136.c)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/single_number_136.cc)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/singleNumber_136.go)      [ref](https://leetcode.com/problems/single-number/)
     
        137. Single Number II (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/SingleNumberII_137.java)      [ref](https://leetcode.com/problems/single-number-ii/)

        139. Word Break (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/wordBreak_139.go)      [ref](https://leetcode.com/problems/word-break/)

        141. Linked List Cycle (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/LinkedListCycle_141.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/linkedListCycle_141.go)      [ref](https://leetcode.com/problems/linked-list-cycle/)
     
        142. Linked List Cycle II (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/LinkedListCycleII_142.java)

        143. Reorder List (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/reorderList_143.go)      [ref](https://leetcode.com/problems/reorder-list/)
     
        144. Binary Tree Preorder Traversal (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/BinaryTreePreorderTraversal_144.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/binaryTreePreorderTraversal_144.go)      [ref](https://leetcode.com/problems/binary-tree-preorder-traversal/)
     
        145. Binary Tree Postorder Traversal (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/hard/BinaryTreePostorderTraversal_145.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/binaryTreePostorderTraversal_145.go)      [ref](https://leetcode.com/problems/binary-tree-postorder-traversal/)
     
        146. LRU Cache (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/LRUCache_146.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/LRUCache_146.go)      [ref](https://leetcode.com/problems/lru-cache/)

        152. Maximum Product Subarray (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/maximumProductSubarray_152.go)      [ref](https://leetcode.com/problems/maximum-product-subarray/)

        153. Find Minimum in Rotated Sorted Array (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/FindMinimumInRotatedSortedArray_153.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/binaryTreePostorderTraversal_145.go)      [ref](https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/)
     
        155. Min Stack (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/MinStack_155.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/minStack_155.go)      [ref](https://leetcode.com/problems/min-stack/)
     
        160. Intersection of Two Linked Lists (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/IntersectionofTwoLinkedLists_160.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/intersectionOfTwoLinkedLists_160.go)      [C](https://bitbucket.org/dodoggyy/leetcode/src/master/C_src/Easy/160_Intersection_of_Two_Linked_Lists/IntersectionOfTwoLinkedLists_160.c)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/intersection_of_two_linked_lists_160.cc)      [ref](https://leetcode.com/problems/intersection-of-two-linked-lists/)

        167. Two Sum II - Input array is sorted (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/TwoSumII_167.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/two_sum_II_167.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/twoSumII_167.go)      [ref](https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/description/)
     
        168. Excel Sheet Column Title (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ExcelSheetColumnTitle_168.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/excel_sheet_column_title_168.cc)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/excelSheetColumnTitle_168.go)      [ref](https://leetcode.com/problems/excel-sheet-column-title/)
     
        169. Majority Element (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/MajorityElement_169.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/majority_element_169.cc)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/majorityElement_169.go)      [ref](https://leetcode.com/problems/majority-element/)
     
        171. Excel Sheet Column Number (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ExcelSheetColumnNumber_171.java)
     
        172. Factorial Trailing Zeroes (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/FactorialTrailingZeroes_172.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/factorial_trailing_zeroes_172.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/factorialTrailingZeroes_172.go)      [ref](https://leetcode.com/problems/factorial-trailing-zeroes/)
     
        175. Combine Two Tables (Easy)
     [mysql](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/CombineTwoTables_175.sql)      [ref](https://leetcode.com/problems/combine-two-tables/)
     
        176. Second Highest Salary (Easy)
     [mysql](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/SecondHighestSalary_176.sql)      [ref](https://leetcode.com/problems/second-highest-salary/)
     
        181. Employees Earning More Than Their Managers (Easy)
     [mysql](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/CombineTwoTables_175.sql)      [ref](https://leetcode.com/problems/employees-earning-more-than-their-managers/)

        182. Duplicate Emails (Easy)
     [mysql](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/DuplicateEmails_182.sql)      [ref](https://leetcode.com/problems/duplicate-emails/)
     
        183. Customers Who Never Order (Easy)
     [mysql](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/CustomersWhoNeverOrder_183.sql)      [ref](https://leetcode.com/problems/customers-who-never-order/)
     
        184. Department Highest Salary (Medium)
     [mysql](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/DepartmentHighestSalary_184.sql)      [ref](https://leetcode.com/problems/department-highest-salary/)
     
        189. Rotate Array (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/RotateArray_189.java)
     
        190. Reverse Bits (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ReverseBits_190.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/reverse_bits_190.cc)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/reverseBits_190.go)      [ref](https://leetcode.com/problems/reverse-bits/)
     
        191. Number of 1 Bits (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/NumberOf1Bits_191.java)
     
        196. Delete Duplicate Emails (Easy)
     [mysql](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/DeleteDuplicateEmails_196.sql)      [ref](https://leetcode.com/problems/delete-duplicate-emails/)
     
        198. House Robber (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/HouseRobber_198.java)    [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/houseRobber_198.go)      [ref](https://leetcode.com/problems/house-robber/)

        200. Number of Islands (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/numberOfIslands_200.go)      [ref](https://leetcode.com/problems/number-of-islands/)
     
        202. Happy Number (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/HappyNumber_202.java)
     
        203. Remove Linked List Elements (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/RemoveLinkedListElements_203.java)
     
        204. Count Primes (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/CountPrimes_204.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/count_primes_204.cc)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/countPrimes_204.go)      [ref](https://leetcode.com/problems/count-primes/)
     
        205. Isomorphic Strings (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/IsomorphicStrings_205.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/isomorphic_strings_205.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/isomorphicStrings_205.go)      [ref](https://leetcode.com/problems/isomorphic-strings/)
     
        206. Reverse Linked List (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ReverseLinkedList_206.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/reverse_linked_list_206.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/reverseLinkedList_206.go)      [ref](https://leetcode.com/problems/reverse-linked-list/)

        207. Course Schedule (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/courseSchedule_207.go)      [ref](https://leetcode.com/problems/course-schedule/)

        208. Implement Trie (Prefix Tree) (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/implementTrie_208.go)      [ref](https://leetcode.com/problems/implement-trie-prefix-tree/)

        210. Course Schedule II (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/courseScheduleII_210.go)      [ref](https://leetcode.com/problems/course-schedule-ii/)

        211. Design Add and Search Words Data Structure (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/designAddAndSearchWordsDataStructure_211.go)      [ref](https://leetcode.com/problems/design-add-and-search-words-data-structure/)
     
        213. House Robber II (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/HouseRobberII_213.java)    [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/HouseRobberII_213.go)      [ref](https://leetcode.com/problems/house-robber-ii/)

        215. Kth Largest Element in an Array (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/KthLargestElementinanArray_215.java)    [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/kthLargestElementInAnArray_215.go)      [ref](https://leetcode.com/problems/kth-largest-element-in-an-array/)
     
        216. Combination Sum III (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/CombinationSumIII_216.java)      [ref](https://leetcode.com/problems/combination-sum-iii/)
     
        217. Contains Duplicate (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ContainsDuplicate_217.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/containsDuplicate_217.go)      [ref](https://leetcode.com/problems/contains-duplicate/)
     
        219. Contains Duplicate II (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ContainsDuplicateII_219.java)
     
        225. Implement Stack using Queues (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ImplementStackUsingQueues_225.java)
     
        226. Invert Binary Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/InvertBinaryTree_226.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/invert_binary_tree_226.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/invertBinaryTree_226.go)      [ref](https://leetcode.com/problems/invert-binary-tree/)
     
        230. Kth Smallest Element in a BST (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/KthSmallestElementInABST_230.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/kthSmallestElementInABST_230.go)      [ref](https://leetcode.com/problems/kth-smallest-element-in-a-bst/)
     
        231. Power of Two (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/PowerofTwo_231.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/power_of_two_231.cc)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/powerOfTwo_231.go)      [ref](https://leetcode.com/problems/power-of-two/)
     
        232. Implement Queue using Stacks (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ImplementQueueUsingStacks_232.java)
     
        234. Palindrome Linked List (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/PalindromeLinkedList_234.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/palindrome_linked_list_234.cc)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/palindromeLinkedList_234.go)      [ref](https://leetcode.com/problems/palindrome-linked-list/)
     
        235. Lowest Common Ancestor of a Binary Search Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/LowestCommonAncestorOfABinarySearchTree_235.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/lowestCommonAncestorOfABinarySearchTree_235.go)      [ref](https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/)
     
        236. Lowest Common Ancestor of a Binary Tree (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/LowestCommonAncestorOfABinaryTree_236.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/lowestCommonAncestorOfABinaryTree_236.go)     [ref](https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/)
     
        237. Delete Node in a Linked List (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/DeleteNodeInALinkedList_237.java)
     
        238. Product of Array Except Self (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/ProductOfArrayExceptSelf_238.java)      [C](https://bitbucket.org/dodoggyy/leetcode/src/master/C_src/Medium/238_Product_of_Array_Except_Self/ProductOfArrayExceptSelf_238.c)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/product_of_array_except_self_238.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/productOfArrayExceptSelf_238.go)      [ref](https://leetcode.com/problems/product-of-array-except-self/)

        241. Different Ways to Add Parentheses (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/differentWaysToAddParentheses_241.go)      [ref](https://leetcode.com/problems/different-ways-to-add-parentheses/)
     
        240. Search a 2D Matrix II (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/Searcha2DMatrixII_240.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/search_a_2D_matrix_II_240.cc)      [ref](https://leetcode.com/problems/search-a-2d-matrix-ii/)
     
        242. Valid Anagram (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ValidAnagram_242.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/valid_anagram_242.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/validAnagram_242.go)      [ref](https://leetcode.com/problems/valid-anagram/)
     
        257. Binary Tree Paths (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/BinaryTreePaths_257.java)
     
        258. Add Digits (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/AddDigits_258.java)
     
        260. Single Number III (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/SingleNumberIII_260.java)      [C](https://bitbucket.org/dodoggyy/leetcode/src/master/C_src/Easy/260_Single_Number_III/SingleNumberIII_260.c)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/single_number_III_260.cc)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/singleNumberIII_260.go)      [ref](https://leetcode.com/problems/single-number-iii/)
     
        263. Ugly Number (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/UglyNumber_263.java)
     
        268. Missing Number (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/MissingNumber_268.java)      [C](https://bitbucket.org/dodoggyy/leetcode/src/master/C_src/Easy/268_Missing_Number/MissingNumber_268.c)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/missing_number_268.cc)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/missingNumber_268.go)      [ref](https://leetcode.com/problems/missing-number/)
     
        278. First Bad Version (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/FirstBadVersion_278.java)      [ref](https://leetcode.com/problems/first-bad-version/)
     
        279. Perfect Squares (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/PerfectSquares_279.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/perfectSquares_279.go)      [ref](https://leetcode.com/problems/perfect-squares/)
     
        283. Move Zeroes (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/MoveZeroes_283.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/move_zeroes_283.cc)      [ref](https://leetcode.com/problems/move-zeroes/)
     
        287. Find the Duplicate Number (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/FindTheDuplicateNumber_287.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/findTheDuplicateNumber_287.go)      [ref](https://leetcode.com/problems/find-the-duplicate-number/)

        289. Game of Life (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/gameOfLife_289.go)      [ref](https://leetcode.com/problems/game-of-life/)
     
        292. Nim Game (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/NimGame_292.java)
     
        297. Serialize and Deserialize Binary Tree (Hard)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/hard/SerializeAndDeserializeBinaryTree_297.java)
     
        300. Longest Increasing Subsequence (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/LongestIncreasingSubsequence_300.java)
     
        303. Range Sum Query - Immutable (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/RangeSumQuery_303.java)
     
        318. Maximum Product of Word Lengths (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/MaximumProductofWordLengths_318.java)      [C](https://bitbucket.org/dodoggyy/leetcode/src/master/C_src/Medium/318_Maximum_Product_of_Word_Lengths/MaximumProductofWordLengths_318.c)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/maximum_product_of_word_lengths_318.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/maximumProductOfWordLengths_318.go)      [ref](https://leetcode.com/problems/maximum-product-of-word-lengths/)
     
        326. Power of Three (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/Easy/PowerOfThree_326.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/powerOfThree_326.go)      [ref](https://leetcode.com/problems/path-sum-iii/)
     
        328. Odd Even Linked List (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/OddEvenLinkedList_328.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/oddEvenLinkedList_328.go)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/odd_even_linked_list_328.cc)      [ref](https://leetcode.com/problems/odd-even-linked-list/)
     
        337. House Robber III (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/HouseRobberIII_337.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/houseRobberIII_337.go)      [ref](https://leetcode.com/problems/house-robber-iii/)
     
        338. Counting Bits (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/CountingBits_338.java)      [C](https://bitbucket.org/dodoggyy/leetcode/src/master/C_src/Medium/338_Counting_Bits/CountingBits_338.c)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/counting_bits_338.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/countingBits_338.go)      [ref](https://leetcode.com/problems/counting-bits/)
     
        342. Power of Four (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/PowerofFour_342.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/power_of_four_342.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/powerOfFour_342.go)      [ref](https://leetcode.com/problems/power-of-four/)
     
        343. Integer Break (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/IntegerBreak_343.java)
     
        344. Reverse String (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ReverseString_344.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/reverse_string_344.cc)      [ref](https://leetcode.com/problems/reverse-string/)

        345. Reverse Vowels of a String (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ReverseVowelsOfAString_345.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/reverseVowelsOfAString_345.go)      [ref](https://leetcode.com/problems/reverse-vowels-of-a-string/)
     
        347. Top K Frequent Elements (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/TopKFrequentElements_347.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/topKFrequentElements_347.go)      [ref](https://leetcode.com/problems/top-k-frequent-elements/)
     
        349. Intersection of Two Arrays (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/IntersectionOfTwoArrays_349.java)
     
        350. Intersection of Two Arrays II (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/IntersectionOfTwoArraysII_350.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/intersectionOfTwoArrays II_350.go)      [ref](https://leetcode.com/problems/intersection-of-two-arrays-ii/)
     
        367. Valid Perfect Square (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ValidPerfectSquare_367.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/valid_perfect_square_367.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/validPerfectSquare_367.go)      [ref](https://leetcode.com/problems/valid-perfect-square/)
     
        371. Sum of Two Integers (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/SumofTwoIntegers_371.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/sum_of_two_integers_371.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/sumOfTwoIntegers_371.go)      [ref](https://leetcode.com/problems/sum-of-two-integers/)
     
        374. Guess Number Higher or Lower (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/GuessNumberHigherOrLower_374.java)
     
        378. Kth Smallest Element in a Sorted Matrix (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/KthSmallestElementinaSortedMatrix_378.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/kthSmallestElementInASortedMatrix_378.go)      [ref](https://leetcode.com/problems/kth-smallest-element-in-a-sorted-matrix/)
     
        383. Ransom Note (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/RansomNote_383.java)

        384. Shuffle an Array (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/shuffleAnArray_384.go)      [ref](https://leetcode.com/problems/shuffle-an-array/)
     
        387. First Unique Character in a String (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/FirstUniqueCharacterInAString_387.java)
     
        389. Find the Difference (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/FindTheDifference_389.java)

        392. Is Subsequence (Easy)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/isSubsequence_392.go)      [ref](https://leetcode.com/problems/is-subsequence/)
     
        400. Nth Digit (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/NthDigit_400.java)
     
        401. Binary Watch (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/BinaryWatch_401.java)
     
        404. Sum of Left Leaves (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/SumOfLeftLeaves_404.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/sumOfLeftLeaves_404.go)      [ref](https://leetcode.com/problems/sum-of-left-leaves/)
     
        405. Convert a Number to Hexadecimal (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ConvertANumberToHexadecimal_405.java)      [C](https://bitbucket.org/dodoggyy/leetcode/src/master/C_src/Easy/405_Convert_a_Number_to_Hexadecimal/ConvertaNumberToHexadecimal_405.c)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/convert_a_number_to_hexadecimal_405.cc)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/convertANumberToHexadecimal_405.go)      [ref](https://leetcode.com/problems/convert-a-number-to-hexadecimal/)

        406. Queue Reconstruction by Height (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/queueReconstructionByHeight_406.go)      [ref](https://leetcode.com/problems/queue-reconstruction-by-height/)
     
        409. Longest Palindrome (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/LongestPalindrome_409.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/longest_palindrome_409.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/longestPalindrome_409.go)      [ref](https://leetcode.com/problems/longest-palindrome/)
     
        412. Fizz Buzz (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/FizzBuzz_412.java)      [ref](https://leetcode.com/problems/path-sum-iii/)
     
        413. Arithmetic Slices (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/ArithmeticSlices_413.java)
     
        415. Add Strings (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/AddStrings_415.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/addStrings_415.go)      [ref](https://leetcode.com/problems/add-strings/)

        416. Partition Equal Subset Sum (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/partitionEqualSubsetSum_416.go)      [ref](https://leetcode.com/problems/partition-equal-subset-sum/)
     
        429. N-ary Tree Level Order Traversal (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/NaryTreeLevelOrderTraversal_429.java)
     
        434. Number of Segments in a String (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/NumberOfSegmentsInAString_434.java)
     
        435. Non-overlapping Intervals (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/NonOverlappingIntervals_435.java)    [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/NonOverlappingIntervals_435.go)      [ref](https://leetcode.com/problems/non-overlapping-intervals/)
     
        437. Path Sum III (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/PathSumIII_437.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/pathSumIII_437.go)      [ref](https://leetcode.com/problems/path-sum-iii/)
     
        438. Find All Anagrams in a String (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/FindAllAnagramsInAString_438.java)
     
        441. Arranging Coins (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ArrangingCoins_441.java)
     
        443. String Compression (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/StringCompression_443.java)
     
        445. Add Two Numbers II (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/AddTwoNumbersII_445.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/add_two_numbers_II_445.cc)      [ref](https://leetcode.com/problems/add-two-numbers-ii/)
     
        448. Find All Numbers Disappeared in an Array (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/FindAllNumbersDisappearedInAnArray_448.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/find_all_numbers_disappeared_in_an_array_448.cc)      [ref](https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/)
     
        449. Serialize and Deserialize BST (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/SerializeAndDeserializeBST_449.java)
     
        451. Sort Characters By Frequency (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/SortCharactersByFrequency_451.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/sortCharactersByFrequency_451.go)      [ref](https://leetcode.com/problems/sort-characters-by-frequency/)

        452. Minimum Number of Arrows to Burst Balloons (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/minimumNumberOfArrowsToBurstBalloons_452.go)      [ref](https://leetcode.com/problems/minimum-number-of-arrows-to-burst-balloons/)

        454. 4Sum II (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/4SumII_454.go)      [ref](https://leetcode.com/problems/4sum-ii/)
     
        455. Assign Cookies (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/AssignCookies_455.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/assignCookies_455.go)      [ref](https://leetcode.com/problems/assign-cookies/)
     
        459. Repeated Substring Pattern (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/RepeatedSubstringPattern_459.java)
     
        461. Hamming Distance (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/HammingDistance_461.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/hamming_distance_461.cc)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/hammingDistance_461.go)      [ref](https://leetcode.com/problems/hamming-distance/)

        462. Minimum Moves to Equal Array Elements II (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/minimumMovesToEqualArrayElementsII_462.go)      [ref](https://leetcode.com/problems/minimum-moves-to-equal-array-elements-ii/)
     
        463. Island Perimeter (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/IslandPerimeter_463.java)
     
        475. Heaters (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/Heaters_475.java)
     
        476. Number Complement (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/NumberComplement_476.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/number_complement_476.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/numberComplement_476.go)      [ref](https://leetcode.com/problems/number-complement/)
     
        482. License Key Formatting (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/LicenseKeyFormatting_482.java)
     
        485. Max Consecutive Ones (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/MaxConsecutiveOnes_485.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/max_consecutive_ones_485.cc)      [ref](https://leetcode.com/problems/max-consecutive-ones/)
     
        492. Construct the Rectangle (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ConstructTheRectangle_492.java)
     
        496. Next Greater Element I (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/NextGreaterElementI_496.java)
     
        500. Keyboard Row (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/KeyboardRow_500.java)
     
        501. Find Mode in Binary Search Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/FindModeInBinarySearchTree_501.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/findModeInBinarySearchTree_501.go)      [ref](https://leetcode.com/problems/find-mode-in-binary-search-tree/)
     
        503. Next Greater Element II (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/NextGreaterElementII_503.java)
     
        504. Base 7 (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/Base7_504.java)      [C](https://bitbucket.org/dodoggyy/leetcode/src/master/C_src/Easy/504_Base_7/Base_7_504.c)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/base_7_504.cc)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/base7_504.go)      [ref](https://leetcode.com/problems/base-7/)
     
        506. Relative Ranks (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/RelativeRanks_506.java)
     
        507. Perfect Number (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/PerfectNumber_507.java)
     
        509. Fibonacci Number (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/FibonacciNumber_509.java)
     
        513. Find Bottom Left Tree Value (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/FindBottomLeftTreeValue_513.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/findBottomLeftTreeValue_513.go)      [ref](https://leetcode.com/problems/find-bottom-left-tree-value/)
     
        520. Detect Capital (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/DetectCapital_520.java)
     
        521. Longest Uncommon Subsequence I (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/LongestUncommonSubsequenceI_521.java)

        524. Longest Word in Dictionary through Deleting (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/LongestWordinDictionarythroughDeleting_524.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/LongestWordInDictionaryThroughDeleting_524.go)      [ref](https://leetcode.com/problems/longest-word-in-dictionary-through-deleting/)
     
        530. Minimum Absolute Difference in BST (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/MinimumAbsoluteDifferenceInBST_530.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/minimumAbsoluteDifferenceInBST_530.go)      [ref](https://leetcode.com/problems/minimum-absolute-difference-in-bst/)
     
        532. K-diff Pairs in an Array (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/KDiffPairsInAnArray_532.java)
     
        538. Convert BST to Greater Tree (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ConvertBSTtoGreaterTree_538.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/convertBSTToGreaterTree_538.go)      [ref](https://leetcode.com/problems/power-of-two/)

        540. Single Element in a Sorted Array (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/SingleElementInASortedArray_540.java)      [ref](https://leetcode.com/problems/single-element-in-a-sorted-array/)
     
        541. Reverse String II (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ReverseStringII_541.java)
     
        543. Diameter of Binary Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/DiameterofBinaryTree_543.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/diameterOfBinaryTree_543.go)      [ref](https://leetcode.com/problems/diameter-of-binary-tree/)
     
        551. Student Attendance Record I (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/StudentAttendanceRecordI_551.java)
     
        557. Reverse Words in a String III (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ReverseWordsInAStringIII_557.java)
     
        558. Quad Tree Intersection (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/QuadTreeIntersection_558.java)
     
        559. Maximum Depth of N-ary Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/MaximumDepthOfNAryTree_559.java)

        560. Subarray Sum Equals K (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/subarraySumEqualsK_560.go)      [ref](https://leetcode.com/problems/subarray-sum-equals-k/)
     
        561. Array Partition I (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ArrayPartitionI_561.java)
     
        563. Binary Tree Tilt (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/BinaryTreeTilt_563.java)
     
        566. Reshape the Matrix (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ReshapeTheMatrix_566.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/reshape_the_matrix_566.cc)      [ref](https://leetcode.com/problems/reshape-the-matrix/)
     
        572. Subtree of Another Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/SubtreeOfAnotherTree_572.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/subtreeOfAnotherTree_572.go)      [ref](https://leetcode.com/problems/subtree-of-another-tree/)
     
        575. Distribute Candies (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/DistributeCandies_575.java)
     
        581. Shortest Unsorted Continuous Subarray (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ShortestUnsortedContinuousSubarray_581.java)
     
        589. N-ary Tree Preorder Traversal (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/NaryTreePreorderTraversal_589.java)
     
        590. N-ary Tree Postorder Traversal (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/NaryTreePostorderTraversal_590.java)
     
        594. Longest Harmonious Subsequence (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/LongestHarmoniousSubsequence_594.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/longestHarmoniousSubsequence_594.go)      [ref](https://leetcode.com/problems/longest-harmonious-subsequence/)
     
        595. Big Countries (Easy)
     [mysql](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/BigCountries_595.sql)      [ref](https://leetcode.com/problems/big-countries/)

        596. Classes More Than 5 Students (Easy)
     [mysql](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ClassesMoreThan5Students_596.sql)      [ref](https://leetcode.com/problems/classes-more-than-5-students/)
     
        598. Range Addition II (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/RangeAdditionII_598.java)
     
        599. Minimum Index Sum of Two Lists (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/MinimumIndexSumOfTwoLists_599.java)
     
        605. Can Place Flowers (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/CanPlaceFlowers_605.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/canPlaceFlowers_605.go)      [ref](https://leetcode.com/problems/can-place-flowers/)
     
        606. Construct String from Binary Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ConstructStringFromBinaryTree_606.java)
     
        617. Merge Two Binary Trees (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/MergeTwoBinaryTrees_617.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/mergeTwoBinaryTrees_617.go)      [ref](https://leetcode.com/problems/merge-two-binary-trees/)

        620. Not Boring Movies (Easy)
     [mysql](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/NotBoringMovies_620.sql)      [ref](https://leetcode.com/problems/not-boring-movies/)
     
        622. Design Circular Queue (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/DesignCircularQueue_622.java)
     
        627. Swap Salary (Easy)
     [mysql](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/SwapSalary_627.sql)      [ref](https://leetcode.com/problems/swap-salary/)
     
        628. Maximum Product of Three Numbers (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/MajorityElement_169.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/maximum_product_of_three_numbers_628.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/maximumProductOfThreeNumbers_628.go)      [ref](https://leetcode.com/problems/maximum-product-of-three-numbers/)

        633. Sum of Square Numbers (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/SumOfSquareNumbers_633.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/sumOfSquareNumbers_633.go)      [ref](https://leetcode.com/problems/sum-of-square-numbers/)
     
        637. Average of Levels in Binary Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/AverageOfLevelsInBinaryTree_637.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/averageOfLevelsInBinaryTree_637.go)      [ref](https://leetcode.com/problems/average-of-levels-in-binary-tree/)
     
        645. Set Mismatch (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/SetMismatch_645.java)      [ref](https://leetcode.com/problems/set-mismatch/)
     
        647. Palindromic Substrings (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/PalindromicSubstrings_647.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/palindromic_substrings_647.cc)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/palindromicSubstrings_647.go)      [ref](https://leetcode.com/problems/palindromic-substrings/)

        653. Two Sum IV - Input is a BST (easy)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/twoSumIV_653.go)      [ref](https://leetcode.com/problems/two-sum-iv-input-is-a-bst/)

        665. Non-decreasing Array (Easy)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/nonDecreasingArray_665.go)      [ref](https://leetcode.com/problems/non-decreasing-array/)
     
        669. Trim a Binary Search Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/TrimABinarySearchTree_669.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/trimABinarySearchTree_669.go)      [ref](https://leetcode.com/problems/trim-a-binary-search-tree/)
     
        671. Second Minimum Node In a Binary Tree (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/SecondMinimumNodeInABinaryTree_671.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/secondMinimumNodeInABinaryTree_671.go)      [ref](https://leetcode.com/problems/second-minimum-node-in-a-binary-tree/)

        677. Map Sum Pairs (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/mapSumPairs_677.go)      [ref](https://leetcode.com/problems/map-sum-pairs/)

        680. Valid Palindrome II (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/ValidPalindromeII_680.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/validPalindromeII_680.go)      [ref](https://leetcode.com/problems/valid-palindrome-ii/)

        684. Redundant Connection (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/redundantConnection_684.go)      [ref](https://leetcode.com/problems/redundant-connection/)
     
        687. Longest Univalue Path (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/LongestUnivaluePath_687.java)     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/longestUnivaluePath_687.go)      [ref](https://leetcode.com/problems/longest-univalue-path/)

        692. Top K Frequent Words (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/topKFrequentWords_692.go)      [ref](https://leetcode.com/problems/top-k-frequent-words/)
     
        693. Binary Number with Alternating Bits (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/BinaryNumberwithAlternatingBits_693.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/binary_number_with_alternating_bits_693.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/binaryNumberWithAlternatingBits_693.go)      [ref](https://leetcode.com/problems/binary-number-with-alternating-bits/)
     
        696. Count Binary Substrings (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/CountBinarySubstrings_696.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/easy/count_binary_substrings_696.cc)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/countBinarySubstrings_696.go)      [ref](https://leetcode.com/problems/count-binary-substrings/)
     
        697. Degree of an Array(Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/DegreeOfAnArray_697.java)

        704. Binary Search (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/BinarySearch_704.java)
     
        705. Design HashSet (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/DesignHashSet_705.java)      [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/designHashSet_705.go)      [ref](https://leetcode.com/problems/design-hashset/)

        706. Design HashMap (Easy)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/designHashMap_706.go)      [ref](https://leetcode.com/problems/design-hashmap/)
     
        707. Design Linked List (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/DesignLinkedList_707.java)
     
        725. Split Linked List in Parts (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/SplitLinkedListInParts_725.java)      [C++](https://bitbucket.org/dodoggyy/leetcode/src/master/Cpp_src/medium/split_linked_list_in_parts_725.cc)      [ref](https://leetcode.com/problems/split-linked-list-in-parts/)
     
        739. Daily Temperatures (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/DailyTemperatures_739.java)

        744. Find Smallest Letter Greater Than Target (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/FindSmallestLetterGreaterThanTarget_744.java)      [ref](https://leetcode.com/problems/find-smallest-letter-greater-than-target/)

        763. Partition Labels (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/partitionLabels_763.go)      [ref](https://leetcode.com/problems/partition-labels/)

        785. Is Graph Bipartite? (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/isGraphBipartite_785.go)      [ref](https://leetcode.com/problems/is-graph-bipartite/)

        876. Possible Bipartition (Easy)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/middleOfTheLinkedList_876.go)      [ref](https://leetcode.com/problems/middle-of-the-linked-list/)

        886. Possible Bipartition (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/medium/possibleBipartition_886.go)      [ref](https://leetcode.com/problems/possible-bipartition/)
     
        983. Minimum Cost For Tickets (Medium)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/medium/MinimumCostForTickets_983.java)

        997. Find the Town Judge (Easy)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/findTheTownJudge_997.go)      [ref](https://leetcode.com/problems/find-the-town-judge/)

        1002. Find Common Characters (Easy)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/findCommonCharacters_1002.go)      [ref](https://leetcode.com/problems/find-common-characters/)

        1091. Shortest Path in Binary Matrix (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/shortestPathInBinaryMatrix_1091.go)      [ref](https://leetcode.com/problems/shortest-path-in-binary-matrix/)
     
        1351. Count Negative Numbers in a Sorted Matrix (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/CountNegativeNumbersInASortedMatrix_1351.java)      [ref](https://leetcode.com/problems/count-negative-numbers-in-a-sorted-matrix/m/problems/find-smallest-letter-greater-than-target/)
     
        1365. How Many Numbers Are Smaller Than the Current Number (Easy)
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/easy/HowManyNumbersAreSmallerThanTheCurrentNumber_1365.java)      [ref](https://leetcode.com/problems/how-many-numbers-are-smaller-than-the-current-number/)

        1646. Get Maximum in Generated Array (Easy)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/getMaximumInGeneratedArray_1646.go)      [ref](https://leetcode.com/problems/get-maximum-in-generated-array/)

        1905. Count Sub Islands (Medium)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/easy/countSubIslands_1905.go)      [ref](https://leetcode.com/problems/count-sub-islands/)
    
    
*   **Design Pattern :**

        1. Singleton Pattern
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/designpattern/SingletonPatternMultiThread.java)

*   **Data Structure :**

        1. Stack
     [Array](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/datastructure/StackViaArray.java)
     [Linked List](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/datastructure/StackViaLinkedList.java)
     
        2. Queue
     [Array](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/datastructure/QueueViaArray.java)
     [Linked List](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/datastructure/QueueViaLinkedList.java)
     
        3. Hash Table
     [java](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/datastructure/MyHashTable.java)
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/dataStructure/hashTable.go)
     
        4. Circular Array
     [Array](https://bitbucket.org/dodoggyy/leetcode/src/master/src/com/datastructure/CircularArray.java)

*   **Cracking the Coding Interview, 6th Edition :**

        1.1 Unique characters
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/isUniqueChars_1_1.go)

        1.2 Valid Anagram
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/validAnagram_1_2.go)

        1.3 Replace Spaces
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/replaceSpaces_1_3.go)

        1.4 Palindrome Permutation
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/palindromePermutation_1_4.go)

        1.5 Is one way
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/oneAway_1_5.go)

        1.6 Basic Compress
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/basicCompress_1_6.go)    [Go test](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/basicCompress_1_6_test.go)

        1.7 Matrix Rotate
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/matrixRotate_1_7.go)    [Go test](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/matrixRotate_1_7_test.go)

        1.9 Is Rotation
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/isRotation_1_9.go)    [Go test](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/isRotation_1_9_test.go)

        2.1 Remove Duplicate
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/removeDuplicates_2_1.go)    [Go test](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/removeDuplicates_2_1_test.go)

        2.2 K From Tail
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/kFromTail_2_2.go)

        2.3 Delete Node
     [Go](https://bitbucket.org/dodoggyy/leetcode/src/master/Golang_src/ctci/deleteNode_2_3.go)


## Authors

* **Chris Lin** - *Java implementation* - [chris](https://www.linkedin.com/in/quanliang-lin-5009ba139/)
